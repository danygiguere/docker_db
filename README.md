# README #

### Local Installation
- First, make sure you have Docker installed on your computer.
- second, run `cp .env.example .env` and add your credentials
- Then make sure Docker is up and run this command: `sh install.sh`

to stop the db docker image, run  `docker-compose down`
to restart it without rebuilding, run  `docker-compose up`
